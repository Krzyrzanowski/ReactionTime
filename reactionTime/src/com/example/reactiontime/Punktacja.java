package com.example.reactiontime;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class Punktacja extends ActionBarActivity {

	TextView napisWynik1; 
	TextView napisWynik2;
	TextView napisWynik3;
	MediaPlayer koniec;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_punktacja);

		koniec = new MediaPlayer();
		koniec = MediaPlayer.create(this,R.raw.zakonczeniedzwieke);
        koniec.start();
		String wynik1 = Double.toString(MainActivity.tablicaWynikow[0]);
		String wynik2 = Double.toString(MainActivity.tablicaWynikow[1]);
		String wynik3 = Double.toString(MainActivity.tablicaWynikow[2]);
		
		
		

		
		napisWynik1=(TextView) findViewById(R.id.napisWynik111);
		napisWynik2=(TextView) findViewById(R.id.napisWynik222);
		napisWynik3=(TextView) findViewById(R.id.napisWynik333);
		
		
		if(MainActivity.tablicaWynikow[0]==5)
			napisWynik1.setText("FALSTART");	
		else napisWynik1.setText(wynik1);
		
		if(MainActivity.tablicaWynikow[1]==5)
			napisWynik2.setText("FALSTART");
		else napisWynik2.setText(wynik2);
		
		if(MainActivity.tablicaWynikow[2]==5)
			napisWynik3.setText("FALSTART");	
		else napisWynik3.setText(wynik3);
		
		
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.punktacja, menu);
		return true;
	}

	@SuppressLint("NewApi") @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		finishAffinity();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    		
    	}
	}
}
