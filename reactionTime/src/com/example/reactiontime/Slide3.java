package com.example.reactiontime;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Slide3 extends ActionBarActivity {
	MediaPlayer falstart;
	MediaPlayer falstart2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide3);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.slide3, menu);
		return true;
	}

	@SuppressLint("NewApi") @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		Slide1.menuTheme.stop();
    		finishAffinity();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    		
    	}
	}
	public void guzikSlide3(View v)
	{
		final Intent naciesniecieGuzika3 = new Intent(this,Sprobujponownie.class);
		startActivity(naciesniecieGuzika3);
		Slide1.guzik.start();
	}
	
	public void dzwiekFalstartu(View v) throws InterruptedException
	{
		falstart = new MediaPlayer();
        falstart = MediaPlayer.create(this,R.raw.shoote);
        falstart2 = new MediaPlayer();
        falstart2 = MediaPlayer.create(this,R.raw.shoote);
        falstart.start();
        Thread.sleep(400);
        falstart2.start();
	}
}
