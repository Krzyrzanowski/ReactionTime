package com.example.reactiontime;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class SplashActivity extends ActionBarActivity {

	
	public static ImageView logo1;
	public static ImageView logo2;
	public static MediaPlayer mpFlashAnimation;
	public static MediaPlayer mpShootingStar;
	final Timer stoperShootw = new Timer();
    final Timer stoperOnYourMarkw = new Timer();
    final Timer niewiemjuz = new Timer();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		 super.onCreate(savedInstanceState);
		 setContentView(R.layout.activity_splash);	
		 SplashActivity.logo2 = (ImageView) findViewById(R.id.idCzyliCzasReakcji);
		
		 SplashActivity.mpFlashAnimation = new MediaPlayer();
		 SplashActivity.mpFlashAnimation = MediaPlayer.create(this,R.raw.flashanimation);
		 SplashActivity.mpShootingStar = new MediaPlayer();
		 SplashActivity.mpShootingStar = MediaPlayer.create(this,R.raw.shootingstar);
		 SplashActivity.logo2.setVisibility(View.INVISIBLE);
		 SplashActivity.logo2.setVisibility(View.VISIBLE);
		 try 
		 {
			Thread.sleep(1000);
		 }
		 catch (InterruptedException e) 
		 {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
		 SplashActivity.mpShootingStar.start();
		 

		final Intent zadanie = new Intent(this,Slide1.class);
		TimerTask task = new TimerTask()
		{
			@Override
			public void run()
			{
				startActivity(zadanie);
			}
		};
		Timer Odliczacz = new Timer();
		Odliczacz.schedule(task, 3000);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{	
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		moveTaskToBack(true);
    		  android.os.Process.killProcess(android.os.Process.myPid());
    		  //System.exit(1);    		super.finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);	
    	}
	}
}
