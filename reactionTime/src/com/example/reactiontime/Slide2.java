package com.example.reactiontime;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

public class Slide2 extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.slide2, menu);
		return true;
	}

	@SuppressLint("NewApi") @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		Slide1.menuTheme.stop();
    		finishAffinity();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    		
    	}
	}
	
	public void guzikSlide2(View v)
	{
		final Intent naciesniecieGuzika2 = new Intent(this,Slide3.class);
		startActivity(naciesniecieGuzika2);
		Slide1.guzik.start();
	}
}
