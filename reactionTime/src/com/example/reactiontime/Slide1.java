package com.example.reactiontime;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Slide1 extends ActionBarActivity 
{
	public static MediaPlayer menuTheme;
	static MediaPlayer guzik;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide1);
		menuTheme = new MediaPlayer();
		menuTheme = MediaPlayer.create(this,R.raw.menuthemee);
		MainActivity.ktoraProba=0;
		menuTheme.start();	
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{

		getMenuInflater().inflate(R.menu.slide1, menu);
		return true;
	}

	@SuppressLint("NewApi") @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		Slide1.menuTheme.stop();
    		finishAffinity();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);	
    	}
	}
	
	public void guzikSlide1(View v)
	{
		final Intent naciesniecieGuzika1 = new Intent(this,Slide2.class);
		startActivity(naciesniecieGuzika1);
		guzik = new MediaPlayer();
        guzik = MediaPlayer.create(this,R.raw.guzikkkkkkkke);
        guzik.start();
	}
}
