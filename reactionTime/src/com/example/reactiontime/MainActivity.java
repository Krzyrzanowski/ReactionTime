package com.example.reactiontime;

import java.util.Timer;
import java.util.TimerTask;

import com.example.reactiontime.MainActivity;
import com.example.reactiontime.DBAdapter;
import com.example.reactiontime.R;

import android.R.string;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.text.format.Time;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.ToggleButton;


@SuppressLint("NewApi") public class MainActivity extends ActionBarActivity  
{
	MediaPlayer onYourMarks;
	MediaPlayer Crowd;
	MediaPlayer set;
	MediaPlayer shoot;
	MediaPlayer shoot2;
	MediaPlayer menuTheme;
	
	static double[] tablicaWynikow = new double[3];
	long teraz;
	long pozniej;
	double realtime = 0;
	double realtime2 ;
	
    Button bGO;
	ImageView run1;
	ImageView run2;
	ImageView run3;
	ImageView run4;
	ImageView run5;
	ImageView run6;
	ImageView run7;
	
	int lNacisniecGuzika = 0;
	boolean czyMoznaNacisnacGuzik1 = false;
	boolean czyMoznaNacisnacGuzik2 = false;
	boolean czyMoznaNacisnacGuzik3 = false;
	boolean czyMoznaSprobujPonownie = false;
	boolean mozliwoscFalstartu = false;
	boolean bylFalstart = false;
	boolean klawiszSprobujPonownieWcisniety= false;
	static boolean  wyjscie = false;
	final Timer stoperSet = new Timer();
	final Timer stoperShoot = new Timer();
    final Timer stoperOnYourMark = new Timer();
    static int ktoraProba;
   
    
    final TimerTask realTimer = new TimerTask() 
    {
			@Override
			public void run() 
			{
				realtime++;
				
			}
		};
	final TimerTask ttshoot = new TimerTask() 
    {	
			@Override
			public void run() 
			{
				if(!bylFalstart)
				{
					mozliwoscFalstartu = false;
	 				shoot.start();
	 				stoperShoot.schedule(realTimer, 0, 10);
	 				czyMoznaNacisnacGuzik3 = true;	
	 				czyMoznaSprobujPonownie = true;
				}
			}
		};
	final TimerTask ttset = new TimerTask() 
    {
		@Override
		public void run() 
		{
			
				int od0do10 = (int) (Math.random()*7000);
				set.start();
				stoperShoot.schedule(ttshoot,3000+od0do10);
				czyMoznaNacisnacGuzik2 = true;
		}
	};
	final TimerTask ttonYourMarks = new TimerTask() 
    {
    	
		@Override
		public void run() 
		{
				int od0do4 = (int) (Math.random()*4000);
				onYourMarks.start();
				stoperSet.schedule(ttset,2000+ od0do4);
				czyMoznaNacisnacGuzik1 = true;
		}
	};
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        akcjaOdNowa();  
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		Crowd.stop();
    		onYourMarks = MediaPlayer.create(this,R.raw.cisza);
    		set = MediaPlayer.create(this,R.raw.cisza);
    		shoot = MediaPlayer.create(this,R.raw.cisza);
    		finishAffinity();
    		return true;
    	case R.id.sprobuj_ponownie:
    		Crowd.stop();
    		super.finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);	
    	}
    }
    
    

    public void przejdzDoPunktacji(View v)
	{
		final Intent naciesniecieGuzika2 = new Intent(this,Punktacja.class);
		startActivity(naciesniecieGuzika2);
		
		Slide1.guzik.start();
	}
    
    
    public void GO(View v) throws InterruptedException
    {
    	if (mozliwoscFalstartu==true)
    	{
    		run2.setVisibility(View.INVISIBLE);
    		run7.setVisibility(View.VISIBLE);
    		bylFalstart = true;
    		shoot.start();
    		Thread.sleep(400);
    		shoot2.start();
    		bGO.setText("FALSTART");
    		mozliwoscFalstartu=false;
    		
        	tablicaWynikow [ktoraProba] = 5;
        	ktoraProba++;
        	if(ktoraProba==3)
        	{
        		Crowd.stop();
        		final Intent naciesniecieGuzika2 = new Intent(this,Punktacja.class);
        		startActivity(naciesniecieGuzika2);
        		Slide1.guzik.start();
        	}
    		
    	}
    	switch(lNacisniecGuzika)
    	{
    	case 0:
    		if (czyMoznaNacisnacGuzik1)
    		{
        		bGO.setText("Przygotuj si� do biegu");
        		run1.setVisibility(View.VISIBLE);	
    		}
    		else lNacisniecGuzika--;
    		break;
    		
    	case 1:
    		if (czyMoznaNacisnacGuzik2)
    		{
        		bGO.setText("GO!");
        		run1.setVisibility(View.INVISIBLE);
        		run2.setVisibility(View.VISIBLE);	
        		mozliwoscFalstartu = true;
    		}
    		else lNacisniecGuzika--;
    		break;
    	case 2:
    		if (czyMoznaNacisnacGuzik3)
    		{
        		run2.setVisibility(View.INVISIBLE);
        		run7.setVisibility(View.VISIBLE);
            	realtime2=realtime;
            	tablicaWynikow [ktoraProba] = realtime2;
            	String wynik = Double.toString(realtime2);
            	bGO.setText(wynik);
            	ktoraProba++;
            	if(ktoraProba==3)
            	{
            		Crowd.stop();
            		final Intent naciesniecieGuzika2 = new Intent(this,Punktacja.class);
            		startActivity(naciesniecieGuzika2);
            		Slide1.guzik.start();
            	}
    		}
    		else lNacisniecGuzika--;
    		break;	
    		
    		
    	default:
    		break;
    	}
    	lNacisniecGuzika++;
    }
    

    
    public void sprobojPonownie(View v)
    {
    	klawiszSprobujPonownieWcisniety = true;
    	akcjaOdNowa();
    }
   

    public void akcjaOdNowa()
    {
		bylFalstart = false;
    	Crowd = new MediaPlayer();
        Crowd = MediaPlayer.create(this,R.raw.crowde);
        Crowd.start();
        onYourMarks = new MediaPlayer();
        onYourMarks = MediaPlayer.create(this,R.raw.onyourmarke);
		Slide1.menuTheme.stop();
		
        set = new MediaPlayer();
        set = MediaPlayer.create(this,R.raw.set);
        shoot = new MediaPlayer();
        shoot = MediaPlayer.create(this,R.raw.shoote);
        shoot2 = new MediaPlayer();
        shoot2 = MediaPlayer.create(this,R.raw.shoote);
        run1 = (ImageView) findViewById(R.id.imageView11);
        run2 = (ImageView) findViewById(R.id.imageView22);
        run3 = (ImageView) findViewById(R.id.imageView33);
        run4 = (ImageView) findViewById(R.id.imageView44);
        run5 = (ImageView) findViewById(R.id.imageView55);
        run6 = (ImageView) findViewById(R.id.imageView66);
        run7 = (ImageView) findViewById(R.id.imageView77);
        bGO = (Button) findViewById(R.id.buttonWynikPunktacja);
    
        realtime = 0;
    	lNacisniecGuzika = 0;
    	czyMoznaNacisnacGuzik1 = false;
    	czyMoznaNacisnacGuzik2 = false;
    	czyMoznaNacisnacGuzik3 = false;
    	mozliwoscFalstartu = false;
    	bylFalstart = false;

        run1.setVisibility(View.INVISIBLE);
        run2.setVisibility(View.INVISIBLE);
        run3.setVisibility(View.INVISIBLE);
        run4.setVisibility(View.INVISIBLE);
        run5.setVisibility(View.INVISIBLE);
        run6.setVisibility(View.INVISIBLE);
        run7.setVisibility(View.INVISIBLE);
        
        stoperOnYourMark.schedule(ttonYourMarks, 2000);
        
        klawiszSprobujPonownieWcisniety=false;
    }

}
