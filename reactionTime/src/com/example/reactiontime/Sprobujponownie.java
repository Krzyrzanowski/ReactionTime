package com.example.reactiontime;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Sprobujponownie extends ActionBarActivity {
	
	static boolean wyjscie;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sprobujponownie);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(Sprobujponownie.wyjscie)
		{
			moveTaskToBack(true);
  		  android.os.Process.killProcess(android.os.Process.myPid());
  		  //System.exit(1);    		super.finish();
		}
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sprobujponownie, menu);
		return true;
	}

	@SuppressLint("NewApi") @Override
	public boolean onOptionsItemSelected(MenuItem item) {switch(item.getItemId())
    	{
    	case R.id.action_exit:
    		Slide1.menuTheme.stop();
    		finishAffinity();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
		}
		
	}
	
	public void guzikSlide4(View v)
	{
		final Intent naciesniecieGuzika3 = new Intent(this,MainActivity.class);
		startActivity(naciesniecieGuzika3);
		Slide1.guzik.start();
		/*MainActivity.onYourMarks.stop();
		MainActivity.Crowd.stop();
		MainActivity.menuTheme.stop();
		MainActivity.shoot.stop();
		MainActivity.shoot2.stop();
		MainActivity.set.stop();*/
		
		
	}
}
